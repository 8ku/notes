# 个人空间与领地性

**个人空间（personal space）**：一个围绕在人们身体周围的可移动的无形的区域，其他人不可以进入这个区域，它界定了人们以多近的距离交往

**领地（territories）**：相对固定的区域，具有可见的界限，规范什么人能相互交往。

领地更多反映<u>群体行为</u>，个人空间更多反映<u>个体行为</u>。

## 个人空间

### 个人空间的功能

个人空间是人与人之间的边界调节机制，这个机制主要有2个目标：

1. **保护功能**。缓冲外界的刺激对情绪和身体的潜在威胁（如，刺激过多，过度唤醒导致的应激、私密性不足、太过亲密或太过疏远、别人的身体攻击）
2. **交流功能**。人际距离决定人们通过何种感觉通道进行交流（如，嗅觉、触觉、视觉、语言）

**人对自己和他人之间的空间距离有不同的期望值，个人空间是人际距离的连续体。**

个人空间仅仅在个体与他人相互作用时才有意义。



### 个人空间的大小

|                           | 适合的关系和活动                 | 感官特性                                                     |
| ------------------------- | -------------------------------- | ------------------------------------------------------------ |
| 亲密距离<br />0~45cm      | 亲密接触、体育运动               | 强烈意识到来自对方的感官刺激（如，气味、热辐射），触觉代替语言作为主要的交流模式 |
| 个人距离<br />45cm~120cm  | 好友间的接触，熟人之间的日常交往 | 比亲密距离更少意识到来自对方的感官刺激；频繁的视线交流；交流更多地通过语言而非触觉实现 |
| 社交距离<br />120cm~360cm | 非个人的和公务性的接触           | 来自对方的感官刺激极少；视觉通道提供的信息不如个人距离情况下那样详细；正常的声音水平；不可能碰触 |
| 公众距离<br />360cm以上   | 个体和公众之间的正式接触         | 没有来自对方的感官刺激；没有细节的视觉输入；夸张的非言语行为用于补充言语交流，在距离上看不清细微表情变化 |



### 决定个人空间的情境因素

- **吸引和人际距离**
  - 女性之间的关系越亲密，相互交往时距离越近，男性之间的距离不会因友谊而改变
- **相似点和人际距离**
  - 相似的人之间比相异的人之间的人际距离更近。人们更愿意和相似的人在更近的距离交往，因为人们预期相似的人对自己的威胁更少。
- **交往方式和人际距离**
  - 有消极色彩的情境能促成更大的个人空间区
  - 高压力情境促成更大空间区
  - 被激怒的情况下，会缩短人际距离

### 决定个人空间的个体差异

- **文化和种族因素**
  - 

