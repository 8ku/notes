# Summary

* [中世纪的遗产和改变欧洲的发现](chapter1.md)
* [文艺复兴](chapter2.md)
* [两种宗教改革](chapter3.md)
    * [基督教笔记](chapter3.1.md)
    * [狩猎女巫相关](chapter3.2.md)
* [宗教战争](chapter4.md)