### 文艺复兴：约1330～1530年

#### 经济繁荣

由于无法抵御法国入侵和此后西班牙的统治，1530年意大利无力支持艺术家们的创作，于是文艺复兴运动结束了。

**海上和陆地的主要贸易路线始发于意大利的城邦国家。意大利城邦由于其自然地理优势，是连接东西方航线的中心。**

佛罗伦萨的纺织业和农产品生产，地中海地区密集的贸易路线，刺激了小规模的工场手工业的发展，也让意大利经济得到飞速发展。14世纪早期银行业的发展也对意大利国内和国际贸易起到很大作用。

8世纪以来，威尼斯一直是东方穆斯林世界和西方基督教世界的贸易和跨文化交流中心。

商业资本主义通过提升市民阶层的地位和影响力，开始消弱贵族的势力。经济的繁荣增强了市民对自身阶层的认同感和自豪感。

#### 社会结构

贫富分化很严重，“肥胖的人”为上流阶层，包括贵族、富有商人阶级和制造商；“普通的”是中产阶级，包括小商贩和匠师等；“微不足道的人”，指工匠和劳动者。

贫富分化的加剧造成阶级差异严重化，建立在互相恭维基础上的、复杂且极其仪式化的礼节强调着阶级差异。上级阶级需要通过各种方式宣示他们的权威。

#### 政治生活

14～15世纪意大利的共和国是一种立宪的寡头政治国家，由最有权势的家族支配。

威尼斯的政体特殊，是共和政体，宪法确保各个集团政治利益的平衡。总督为参议院选举出来的终身职位，称作“道奇”。大议会和参议院都由贵族阶级组成。

意大利各地的独裁者和寡头形成了小而高效的官僚体制，相互联姻的权贵家族通过购买等方式，获取政府部门的职位。

君主和寡头有数量庞大的随从和官员侍奉。

意大利强国都极具侵略性，如佛罗伦萨、米兰、威尼斯、那不勒斯，教皇国等。

#### 佛罗伦萨

地理位置：地处内陆，地势平缓，阿诺河横穿城市，占据了比萨港口。

文化：神职人员、律师及公证人的基本技能是熟知罗马法和拉丁语。美第奇家族鼓励具有有强烈世俗色彩的文化运动。使市民生活深受基督教文化传统和世俗文化传统的熏陶。

经济：手工业传统和与奢侈品生产密切相关的经济，让佛罗伦萨宽容地接受艺术革新。

教育：有良好的教育基础，识字人口比例欧洲最高，吸引了其他地区的人才。

### 活力四射的文化

#### 重新发现古典知识

托斯卡纳诗人**彼得拉克**是最早重新发现并赞扬古典拉丁语文献、且最有影响力的人之一。

#### 人文主义

罗马人用“人文”来描述智慧与美德的结合。被指带思想解放的研究和古代的“七艺”：语法、逻辑、算术、几何、音乐、天文以及修辞学。

老师和学生把重点从传统的法律、医药和神学转到了拉丁语语法、修辞学和形而上学上，成了“人文主义者”。

人文主义者尊崇古典文明，批判中世纪的经院哲学。鄙视无用的哲学思考和跟人类无关的知识（例如动物的知识）。

人文主义者认为哲学家或人文主义者应该是能管理国家的智者。

#### 文艺复兴与宗教

人文主义者对古典主义的故事进行神学的解读。

在宗教问题上，大体是中世纪的延续。

#### 文艺复兴时期的男性和女性

文艺复兴时期的文学和诗歌，充满对自然、美和理性的推崇，**个体的人**是它们最关注的对象。

自传文学体裁、肖像画和自画像的出现，反映了对个人的赞颂。

注重自我形象让意大利的贵族更注意个人卫生和言行举止。

文艺复兴更多的是强调男性的价值，女性是从属于男人的。女性的社会和个人选择可能变得更狭小。女性在社会中更类似一种象征性、仪式性的角色。

### 文艺复兴时期的艺术

艺术家得到的支持和声望为他们创造出良性的社会环境，让他们取得了超凡的成就。

#### 建筑

15世纪，佛罗伦萨的建筑业处于鼎盛时期。

文艺复兴时期的建筑强调高雅简洁，很多圆形石柱、拱门、华丽无比的楼梯。

14世纪艺术家们用圆形来设计教学。

#### 资助者与艺术

富裕的信仰人文主义的家庭相信，资助艺术创作似乎可以从道德上确立他们的领袖地位，因此有利于获取统治权力。

15世纪漫长的经济萧条可能促进了艺术的发展。贵族世家发现商业和制造业无法获得足够利润，干脆投资于绘画和雕塑。由于生产性资本投入的减少，导致经济衰退进一步恶化。

资助者不但指定绘画内容，还会对相关细节做要求，如指定的人物，艺术品的规格和价格，艺术品创作人数，要求资助者本人出现在画中等。

#### 文艺复兴时期的艺术家

中世纪时期，绘画被认为是某种“机械”技艺。文艺复兴时期，艺术家的地位有所提升。

#### 绘画和雕塑

自然成为绘画中的主体，人们逐渐注重描绘现实世界。

艺术家必须能够体现所画人物的情感和激情。利用大量已成定式的手法来展现人物的情绪，利用颜色象征意义，用更微妙和自然的深色填充画面。

透视理论得到发展。1425年，马萨乔首先应用精准的透视法，创作划时代意义的《圣三位一体》。

文艺复兴时期的意大利人在他们所看到的一切中寻找宗教内涵，因此他们的宗教生活、公共生活和私人生活往往重叠在一起。

宗教主题占据主导地位：**玛利亚**为最受欢迎的人物，其次是**基督和其他圣徒（最受欢迎的圣徒是施洗者约翰）**。

古典神话场景是赞助人坚信能为作品增色而出现的，同时，世俗主题的绘画也在增加。

#### 文艺复兴盛期的风格

时间：1490～1530年

意大利先后遭遇法国入侵和西班牙人统治，丧失大部分的经济和政治活力，艺术家们转向向教会寻求资助。

文艺复兴盛期的画家更注重**用视觉效果震撼观众**，这和教皇试图在艺术家的帮助下试图恢复人们对教会的信任有关。（16世纪20年代，教皇因出售赎罪券备受谴责）

**达·芬奇的《最后的晚餐》是第一幅展现文艺复兴盛期风格的巨作**（又被称为“雄伟风格”）。

**风格主义**：特征是拉长的比例、夸张的戏剧性场面和带来整体性的情感效果的丰富的细节。

**代表建筑：圣彼得大教堂**

风格主义的作品以充满想象力的扭曲处理和不安定感营造出使人难以平静的画面，蕴藏着新的不确定性。**希望重现古典的完美风格**。

### 文艺复兴的结束

15世纪晚期，

- 意大利更无力抵御外国入侵
- 西班牙和法国在意大利半岛上打了几仗
- 西班牙在美洲发起探险和殖民活动
- 欧洲西北部贸易增长，制造业发展，将经济和文化活力推向大西洋，推向西班牙及欧洲西北部，特别是英格兰、法兰西、荷兰，也推向新大陆

#### 衰退的经济

15世纪下半叶，意大利北部城邦的经济衰退破坏了文艺复兴繁荣的物质基础，也使当时**整个地中海地区失去了经济领先地位**。

土耳其人占领了热那亚（意大利北部港口）在黑海的贸易点、通往亚洲的传统航线和爱琴海内部的航路。

威尼斯是唯一持续繁荣的国家，因为它和土耳其人签订协议，获得与东方贸易的垄断权。

长期繁荣的佛罗伦萨丝绸和羊毛产业，面临法国和荷兰生产者和西北欧商人的激烈竞争。

葡萄牙、西班牙、英格兰、荷兰商人在新世界寻找新产品，英格兰、荷兰及葡萄牙大型商船在竞争中完胜佛罗伦萨和热那亚，逐步击败威尼斯。

#### 外国入侵

1454年，佛罗伦萨、米兰、威尼斯签署《洛迪和约》，抵制了土耳其和法国的势力增长，带来40年相对和平的时间，诞生了一些文艺复兴时期最高的艺术成就。

……

#### 马基雅维利（Machiavelli）

意大利政治哲学大师，《君主论》作者。

马基雅维利主义：实力原则、不择手段、双重角色。

马基雅维利信奉政治统率力，认为不管政府的形式是君主还是共和，政府的目标都是给城邦带来稳定。只有建立“良好的法律和制度”，才能帮助公民重新树立起作为公民的责任感，前提是有强大的军事力量的支持。

《君主论》彻底分割了现实主义与理想主义，马基雅维利认为君主应该做的是将善良与邪恶作为一种夺取权力的手段，而不是目标本身。

**实用主义**是马基雅维利所遵循的主要原则。

#### 城邦的衰落

16世纪头30年，在意大利的外国军队边互相争斗边与意大利各个城邦结盟抗敌。

除了威尼斯，意大利所有城邦国家都或多或少受制于神圣罗马皇帝查理五世。

被商人消弱了政治权力的贵族，利用混乱时局，又重新在一些城市恢复了显赫地位，对抗新兴阶级。

本时期，受到法国入侵冲击的艺术充满了焦虑和悲观。

1530年后，西班牙的影响增加，艺术家们更难找到资助者。

#### 别处的驱动

随着意大利本土的衰退，文艺复兴时期的乐观气氛转移到了欧洲中部和北部。很多人文主义者和艺术家开始移民到阿尔卑斯山以北的土地上。